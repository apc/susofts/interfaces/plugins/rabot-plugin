/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef RABOTPLUGIN_HPP
#define RABOTPLUGIN_HPP

#include <interface/SPluginInterface.hpp>

class RabotPlugin : public QObject, public SPluginInterface
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID SPluginInterfaceIID)
	Q_INTERFACES(SPluginInterface)

public:
	RabotPlugin(QObject *parent = nullptr);
	virtual ~RabotPlugin() override;

	virtual const QString &name() const noexcept override { return RabotPlugin::_name(); }
	virtual QIcon icon() const override;

	virtual void init() override;
	virtual void terminate() override;

	virtual bool hasConfigInterface() const noexcept override { return true; }
	virtual SConfigWidget *configInterface(QWidget *parent = nullptr) override;
	virtual bool hasGraphicalInterface() const noexcept override { return false; }
	virtual SGraphicalWidget *graphicalInterface(QWidget *parent = nullptr) override;
	virtual bool hasLaunchInterface() const noexcept override { return true; }
	virtual SLaunchObject *launchInterface(QObject *parent = nullptr) override;

	virtual QString getExtensions() const noexcept override { return tr("Rabot (*.rab)"); }
	virtual bool isMonoInstance() const noexcept override { return true; }

	// not exposed methods
public:
	static const QString &_name();
	static const QString &_baseMimetype();

private:
	class _RabotPlugin_pimpl;
	std::unique_ptr<_RabotPlugin_pimpl> _pimpl;
};

#endif // RABOTPLUGIN_HPP
