#include "RabotPlugin.hpp"

#include <Logger.hpp>

#include "Version.hpp"
#include "trinity/RabotConfig.hpp"
#include "trinity/RabotGraphicalWidget.hpp"
#include "trinity/RabotLaunchObject.hpp"

class RabotPlugin::_RabotPlugin_pimpl
{
public:
};

RabotPlugin::RabotPlugin(QObject *parent) : QObject(parent), SPluginInterface(), _pimpl(std::make_unique<_RabotPlugin_pimpl>())
{
}

RabotPlugin::~RabotPlugin() = default;

QIcon RabotPlugin::icon() const
{
	return QIcon(":/icons/rabot");
}

void RabotPlugin::init()
{
	logDebug() << "Plugin Rabot loaded";
}

void RabotPlugin::terminate()
{
	logDebug() << "Plugin Rabot unloaded";
}

SConfigWidget *RabotPlugin::configInterface(QWidget *parent)
{
	logDebug() << "loading RabotPlugin::configInterface (RabotConfig)";
	return new RabotConfig(this, parent);
}

SGraphicalWidget *RabotPlugin::graphicalInterface(QWidget *parent)
{
	logDebug() << "loading Rabotlugin::graphicalInterface (RabotGraphicalWidget)";
	//return new RabotGraphicalWidget(this, parent);
	return nullptr;
}

SLaunchObject *RabotPlugin::launchInterface(QObject *parent)
{
	logDebug() << "loading RabotPlugin::launchInterface (RabotLaunchObject)";
	return new RabotLaunchObject(this, parent);
}

const QString &RabotPlugin::_name()
{
	static const QString _sname = QString::fromStdString(getPluginName());
	return _sname;
}

const QString &RabotPlugin::_baseMimetype()
{
	static const QString _sbaseMimetype = "application/vnd.cern.susoft.surveypad.plugin.rabot";
	return _sbaseMimetype;
}
