/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef RABOTCONFIG_HPP
#define RABOTCONFIG_HPP

#include <memory>

#include <interface/SConfigInterface.hpp>

namespace Ui
{
class RabotConfig;
}

struct RabotConfigObject : public SConfigObject
{
	virtual ~RabotConfigObject() override = default;

	bool operator==(const SConfigObject &o) const noexcept override;

	virtual void write() const override;
	virtual void read() override;

	/** Path to Rabot executable */
	const QString rabotVersion = "1.4.1"; // Cannot be modified by a plugin user
	QString rabotPath = "C:/Program Files/SUSoft/PyRabot/" + rabotVersion + "/PyRabot.exe";
};

class RabotConfig : public SConfigWidget
{
	Q_OBJECT

public:
	RabotConfig(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~RabotConfig();

	// SConfigInterface
	virtual SConfigObject *config() const override;
	virtual void setConfig(const SConfigObject *conf) override;
	virtual void reset() override;
	virtual void restoreDefaults() override;

private slots:
	void selectPath();

private:
	std::unique_ptr<Ui::RabotConfig> ui;
};

#endif // RABOTCONFIG_HPP
