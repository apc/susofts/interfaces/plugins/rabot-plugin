#include "RabotGraphicalWidget.hpp"

#include <QFileInfo>
#include <QTabWidget>

#include "RabotPlugin.hpp"

class RabotGraphicalWidget::_RabotGraphicalWidget_pimpl
{
public:
};

RabotGraphicalWidget::RabotGraphicalWidget(SPluginInterface *owner, QWidget *parent) :
	STabInterface(owner, parent), _pimpl(std::make_unique<_RabotGraphicalWidget_pimpl>())
{
}

RabotGraphicalWidget::~RabotGraphicalWidget() = default;

QString RabotGraphicalWidget::getPath() const
{
	auto *currentTab = qobject_cast<SGraphicalWidget *>(tab()->currentWidget());
	if (currentTab)
		return QFileInfo(currentTab->getPath()).absoluteFilePath();
	return QString();
}
