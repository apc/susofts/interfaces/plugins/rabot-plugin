/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef RABOTLAUNCHOBJECT_HPP
#define RABOTLAUNCHOBJECT_HPP

#include <interface/ProcessLauncherObject.hpp>

class RabotLaunchObject : public ProcessLauncherObject
{
	Q_OBJECT

public:
	RabotLaunchObject(SPluginInterface *owner, QObject *parent = nullptr) : ProcessLauncherObject(owner, parent) {}
	virtual ~RabotLaunchObject() override = default;

	// SLaunchObject
	virtual QString exepath() const override;

public slots:
	// ProcessLauncherObject
	virtual void launch(const QString &file) override;
};

#endif // RABOTLAUNCHOBJECT_HPP
