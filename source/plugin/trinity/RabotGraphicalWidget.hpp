/*
© Copyright CERN 2000-2018. All rigths reserved. This software is released under a CERN proprietary software licence.
Any permission to use it shall be granted in writing. Request shall be adressed to CERN through mail-KT@cern.ch
*/

#ifndef RABOTGRAPHICALWIDGET_HPP
#define RABOTGRAPHICALWIDGET_HPP

#include <interface/STabInterface.hpp>

class RabotGraphicalWidget : public STabInterface
{
	Q_OBJECT

public:
	RabotGraphicalWidget(SPluginInterface *owner, QWidget *parent = nullptr);
	virtual ~RabotGraphicalWidget() override;

	// SgraphicalWidget
	virtual QString getPath() const override;

public slots:
	// SGraphicalWidget
	virtual void updateUi(const QString &) override {}
	virtual void aboutTorun() override {}
	virtual void runFinished(bool) override {}

private:
	class _RabotGraphicalWidget_pimpl;
	std::unique_ptr<_RabotGraphicalWidget_pimpl> _pimpl;
};

#endif // RABOTGRAPHICALWIDGET_HPP
