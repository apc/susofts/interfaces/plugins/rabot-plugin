#include "RabotLaunchObject.hpp"

#include <QProcess>

#include <ShareablePoints/SPIOException.hpp>
#include <utils/SettingsManager.hpp>

#include "RabotPlugin.hpp"
#include "trinity/RabotConfig.hpp"

QString RabotLaunchObject::exepath() const
{
	auto conf = SettingsManager::settings().settings<RabotConfigObject>(RabotPlugin::_name());
	return conf.rabotPath;
}

void RabotLaunchObject::launch(const QString &file)
{
	process().setArguments({file});
	process().setProgram(exepath());
	ProcessLauncherObject::launch(file);
}
