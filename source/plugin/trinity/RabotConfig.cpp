#include "RabotConfig.hpp"
#include "ui_RabotConfig.h"

#include <QDir>
#include <QFileDialog>
#include <QSettings>

#include "RabotPlugin.hpp"
#include "Version.hpp"

bool RabotConfigObject::operator==(const SConfigObject &o) const noexcept
{
	if (!SConfigObject::operator==(o))
		return false;
	const RabotConfigObject &oc = static_cast<const RabotConfigObject &>(o);
	return rabotPath == oc.rabotPath;
}

void RabotConfigObject::write() const
{
	QSettings settings;
	settings.beginGroup(RabotPlugin::_name());

	settings.setValue("rabotPath_v" + rabotVersion, rabotPath);

	settings.endGroup();
}

void RabotConfigObject::read()
{
	QSettings settings;
	settings.beginGroup(RabotPlugin::_name());

	rabotPath = settings.value("rabotPath_v" + rabotVersion, "C:/Program Files/SUSoft/Rabot/" + rabotVersion + "/Rabot.exe").toString();

	settings.endGroup();
}

RabotConfig::RabotConfig(SPluginInterface *owner, QWidget *parent) : SConfigWidget(owner, parent), ui(std::make_unique<Ui::RabotConfig>())
{
	ui->setupUi(this);
	ui->lblVersion->setText(ui->lblVersion->text().arg(QString::fromStdString(getVersion())));
}

RabotConfig::~RabotConfig() = default;

SConfigObject *RabotConfig::config() const
{
	auto *tmp = new RabotConfigObject();
	tmp->rabotPath = QDir::fromNativeSeparators(ui->lineExePath->text());
	return tmp;
}

void RabotConfig::setConfig(const SConfigObject *conf)
{
	const auto *config = dynamic_cast<const RabotConfigObject *>(conf);
	if (config)
	{
		ui->lineExePath->setText(QDir::toNativeSeparators(config->rabotPath));
	}
}

void RabotConfig::reset()
{
	RabotConfigObject config;
	config.read();
	setConfig(&config);
}

void RabotConfig::restoreDefaults()
{
	RabotConfigObject config;
	setConfig(&config);
}

void RabotConfig::selectPath()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Rabot executable file"), ui->lineExePath->text(), tr("PyRabot.exe (PyRabot.exe);;Executable (*.exe);;Any (*.*)"));
	if (!filename.isEmpty())
		ui->lineExePath->setText(QDir::toNativeSeparators(filename));
}
