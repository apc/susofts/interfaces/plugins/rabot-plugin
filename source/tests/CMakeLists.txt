set(TEST_NAME testsRabotPlugin)

set(TESTS_SOURCES
	main.cpp
	test_plugin.cpp
)
set(TESTS_HEADERS
)

# As the plugin creates a DLL with just the plugin interface exposed,
# if we want to test inner classes, we have to integrate them statically here...
set(PLUGIN_SOURCES "")
foreach(f ${PLUGIN_ALL_SRCES})
	if(NOT IS_ABSOLUTE ${f})
		set(f "../${f}")
	endif()
	list(APPEND PLUGIN_SOURCES ${f})
endforeach()
add_executable(${TEST_NAME}
	# plugin
	${PLUGIN_SOURCES}
	# tests
	${TESTS_HEADERS}
	${TESTS_SOURCES}
)
target_compile_definitions(${TEST_NAME} PRIVATE
	QSCINTILLA_DLL
	UIPlugins_IMPORT
	PROJECT_NAME="${PROJECT_NAME}"
)
target_include_directories(${TEST_NAME} PRIVATE
	${CMAKE_CURRENT_BINARY_DIR}
	${PROJECT_SOURCE_DIR}/plugin
	# TUT
	${TUT_INCLUDE_PATH}
	# SurveyLib
	${SURVEYLIB_ROOT}/source/Logs
	${SURVEYLIB_ROOT}/source/Plugins
	${SURVEYLIB_ROOT}/source/Tools/headers
	# SUGL
	${SUGL_ROOT}/source/UIPlugins
)
target_link_libraries(${TEST_NAME}
	# Qt
	Qt5::Core
	Qt5::Gui
	Qt5::Widgets
	# QScintilla
	qscintilla2_qt5
	# SurveyLib
	Plugins
	Tools
	# SUGL
	UIPlugins
)

get_target_property(QSCINTILLA_DLL_DEBUG qscintilla2_qt5 IMPORTED_LOCATION_DEBUG)	
get_target_property(QSCINTILLA_DLL qscintilla2_qt5 IMPORTED_LOCATION_RELEASE)
if(WIN32)
	deployqt(${TEST_NAME} "$<TARGET_FILE_DIR:${TEST_NAME}>" "$<$<CONFIG:debug>:${QSCINTILLA_DLL_DEBUG}>$<$<CONFIG:release>:${QSCINTILLA_DLL}>")
	deployqt(${TEST_NAME} "$<TARGET_FILE_DIR:${TEST_NAME}>" "$<TARGET_FILE:UIPlugins>")
	deployqt(${TEST_NAME} "$<TARGET_FILE_DIR:${TEST_NAME}>" "$<TARGET_FILE:${PROJECT_NAME}>")
endif()
post_build_copy_dlls(${TEST_NAME} "${QSCINTILLA_DLL_DEBUG}" "${QSCINTILLA_DLL}" "")
post_build_copy_dlls(${TEST_NAME} "$<TARGET_FILE:UIPlugins>" "$<TARGET_FILE:UIPlugins>" "" TRUE)
post_build_copy_dlls(${TEST_NAME} "$<TARGET_FILE:${PROJECT_NAME}>" "$<TARGET_FILE:${PROJECT_NAME}>" "plugins" TRUE)

add_test(
	NAME ${TEST_NAME}
	COMMAND $<TARGET_FILE_NAME:${TEST_NAME}>
	WORKING_DIRECTORY $<TARGET_FILE_DIR:${TEST_NAME}>
)

# IDE
source_group("tests" FILES ${TESTS_HEADERS} ${TESTS_SOURCES})
source_group("_plugin" FILES ${PLUGIN_SOURCES})
